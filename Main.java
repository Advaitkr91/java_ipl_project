package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Main {
    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int WINNER = 10;
    public static final int MATCH_ID = 0;
    public static final int BOWLING_TEAM = 3;
    public static final int EXTRA_RUNS = 16;
    public static  final int TOTALRUNS = 17;
    public static final int BOWLERS = 8;
    public static final int BATSMAN = 6;
    public static final int BATSMAN_RUN = 15;
    public static void main(String[] args) throws IOException{
        List<Match> matches_data = getMatchesData();
        List<Delivery> deliveries_data = getDeliveryData();


        findNumberOfMatchesPlayedPerYear(matches_data);
        findNumberOfMatchesWinByEachTeam(matches_data);
        findExtraRun2016PerTeam(matches_data,deliveries_data);
        findEconomyOfBowlersIn2015(matches_data,deliveries_data);
        findRunScoredByDhoniIn2015(matches_data,deliveries_data);



    }
    private static List<Match> getMatchesData() throws IOException {

        List<Match> matches = new ArrayList();
        String linematch = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("matches.csv"));

            br.readLine();

            while ((linematch = br.readLine()) != null) {
                Match match = new Match();
                String[] matchData = linematch.split(",");
                match.setId(Integer.parseInt(matchData[ID]));
                match.setSeason(Integer.parseInt(matchData[SEASON]));
                match.setWinner(matchData[WINNER]);
                matches.add(match);
            }

        }catch (Exception e) {
              e.printStackTrace();
        }
        return matches;

    }
    private static List<Delivery> getDeliveryData() throws IOException {

        List<Delivery> deliveries = new ArrayList();
        String linematch = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("deliveries.csv"));

            br.readLine();

            while ((linematch = br.readLine()) != null) {
                Delivery delivery = new Delivery();
                String[] delivery_Data = linematch.split(",");
                delivery.setMatchId(Integer.parseInt(delivery_Data[MATCH_ID]));
                delivery.setBowlingTeam(delivery_Data[BOWLING_TEAM]);
                delivery.setExtraRuns(Integer.parseInt(delivery_Data[EXTRA_RUNS]));
                delivery.setBowlerName(delivery_Data[BOWLERS]);
                delivery.setTotalRuns(Integer.parseInt(delivery_Data[TOTALRUNS]));
                delivery.setBatsmanName(delivery_Data[BATSMAN]);
                delivery.setBatsmanRuns(Integer.parseInt(delivery_Data[BATSMAN_RUN]));
                deliveries.add(delivery);
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println(deliveries);
        //System.out.println("yes");
        return deliveries;

    }


  public static void findNumberOfMatchesPlayedPerYear(List<Match>matches_data){
      System.out.println(matches_data);
      HashMap<Integer, Integer> TotalOfMatchesPlayedPerYear
              = new HashMap<Integer, Integer>();
        for(int i=0;i<matches_data.size();i++){
            Match match = matches_data.get(i);
            int season = match.getSeason();
            if(TotalOfMatchesPlayedPerYear.containsKey(season)){

                  TotalOfMatchesPlayedPerYear.put(season,TotalOfMatchesPlayedPerYear.get(season) + 1);
            }
            else{

                TotalOfMatchesPlayedPerYear.put(season,1);
            }

        }
      System.out.println(TotalOfMatchesPlayedPerYear);
    }


    public static void findNumberOfMatchesWinByEachTeam(List<Match>matches_data){

        HashMap<String, Integer> NumberOfMatchesWinByEachTeam
                = new HashMap<String, Integer>();
        for(int i=0;i<matches_data.size();i++){
            Match match = matches_data.get(i);
            String winner = match.getWinner();
            if(!winner.equals("")) {
                if (NumberOfMatchesWinByEachTeam.containsKey(winner)) {
                    NumberOfMatchesWinByEachTeam.put(winner, NumberOfMatchesWinByEachTeam.get(winner) + 1);
                } else {
                    NumberOfMatchesWinByEachTeam.put(winner, 1);

                }

            }
        }
        System.out.println(NumberOfMatchesWinByEachTeam);

    }


    public static void findExtraRun2016PerTeam(List<Match>matches_data,List<Delivery>deliveries_data){

        HashMap<String, Integer> ExtraRunPerTeam
                = new HashMap<String, Integer>();

        for(int i=0;i<matches_data.size();i++){

            Match match = matches_data.get(i);
            int season = match.getSeason();
            int match_id = match.getId();
            if(season == 2016){


                for(int j=0;j<deliveries_data.size();j++){
                    Delivery delivery = deliveries_data.get(j);
                    int delivery_id = delivery.getMatchId();
                    int extra_run = delivery.getExtraRuns();
                    if(delivery_id == match_id){
                        String team = delivery.getBowlingTeam();
                        if(ExtraRunPerTeam.containsKey(team)){

                            ExtraRunPerTeam.put(team,ExtraRunPerTeam.get(team) + extra_run);
                        }
                        else{

                            ExtraRunPerTeam.put(team,0);
                        }
                    }

                }



            }


        }

        System.out.println(ExtraRunPerTeam);
    }

    public static void   findEconomyOfBowlersIn2015(List<Match>matches_data,List<Delivery>deliveries_data){

        HashMap<String, Float> EconomyOfEachBowler
                = new HashMap<>();
        HashMap<String, Integer> TotalRun
                = new HashMap<String, Integer>();
        HashMap<String, Integer> TotalBall
                = new HashMap<String, Integer>();

        for(int i=0;i<matches_data.size();i++){

            Match match = matches_data.get(i);
            int season = match.getSeason();
            if(season == 2015){

                int id = match.getId();
                for(int j=0;j<deliveries_data.size();j++){

                    Delivery delivery = deliveries_data.get(j);
                    int delivery_id = delivery.getMatchId();
                    int totalRun = delivery.getTotalRuns();
                    //int totalBalls = delivery.getBall();
                    //System.out.println(totalRun);
                    if(id == delivery_id){

                        String bowlerName = delivery.getBowlerName();
                        if(TotalRun.containsKey(bowlerName)){

                            TotalRun.put(bowlerName,TotalRun.get(bowlerName) +totalRun );
                        }
                        else{

                            TotalRun.put(bowlerName,1);
                        }

                        if(TotalBall.containsKey(bowlerName)){

                            TotalBall.put(bowlerName,TotalBall.get(bowlerName) + 1);
                        }
                        else{

                            TotalBall.put(bowlerName,0);
                        }


                    }



                }






            }
        }

        TotalRun.forEach((key, value) ->{

            if(TotalBall.containsKey(key)){
                //int semi_val = total_ball.get(key);
                //String val = total_ball.getValue();
                EconomyOfEachBowler.put(key, (float)(value/(float)((TotalBall.get(key)/6))));

            }

        });
        System.out.println(EconomyOfEachBowler);

       }

    public static void  findRunScoredByDhoniIn2015(List<Match>matches_data,List<Delivery>deliveries_data){
        HashMap<String, Integer> TotalRunScored
                = new HashMap<>();
        int TotalRunByDhoni = 0;

        for(int i=0;i< matches_data.size();i++){

            Match match = matches_data.get(i);
            int season = match.getSeason();
            if(season == 2012){

                int id = match.getId();
                for(int j=0;j<deliveries_data.size();j++){

                    Delivery delivery = deliveries_data.get(j);
                    int delivery_id = delivery.getMatchId();

                    if(id == delivery_id){
                        String batsman = delivery.getBatsmanName();


                        if(batsman.equals("MS Dhoni"))
                        {

                            int batsmanRun = delivery.getBatsmanRuns();

                            TotalRunByDhoni += batsmanRun;
                            TotalRunScored.put(batsman,TotalRunByDhoni);

                        }

                    }
                }

            }

        }
        System.out.println(TotalRunScored);

     }
    }





